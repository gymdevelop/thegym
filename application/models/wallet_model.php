<?php
class Wallet_model extends CI_Model {

	public function __construct()
	{
		// empty constructor
	}
	
	public function get_actual_credit_by_user_id( $id_user )
	{

		$this->db->select('actual_credit');
		$this->db->from('wallet');
		$this->db->where('fk_id_user', $id_user);		

		$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
			$row = $query->row();

			return ($row->actual_credit);
		}
		
		// no data retrieved, some error had to occur
		return -1; 
	}
	
	public function get_wallet_id_by_user_id( $id_user )
	{

		$this->db->select('id_wallet');
		$this->db->from('wallet');
		$this->db->where('fk_id_user', $id_user);	

		$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
			$row = $query->row();

			return ($row->id_wallet);
		}
		
		// no data retrieved, some error had to occur
		return -1; 
	}
	
	/*
	 * Update actual credit value of a certain wallet by adding new value to actual one
	 */
	public function add_credit_to_wallet( $id_wallet, $value)
	{
		
		// retrieve actual value
		$this->db->select('actual_credit');
		$this->db->from('wallet');
		$this->db->where('id_wallet', $id_wallet);	

		$query = $this->db->get();
		$row = $query->row();
		$actual_credit = $row->actual_credit;
		
		$new_value = $actual_credit + $value;
		
		// update with new value
		$wallet_data = array(
               'actual_credit' => $new_value
            );

		$this->db->where('id_wallet', $id_wallet);
		$this->db->update('wallet', $wallet_data); 		
	}

}

/* End of file wallet_model.php */
/* Location: ./application/models/wallet_model.php */

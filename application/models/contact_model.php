<?php

class Contact_model extends CI_Model {

    public function __construct() {
        //parent::__construct();
       // $this->load->database();
    }

    public function get_gym_phone_number_and_address() {
        $this->db->select('phone_number, address');
        $this->db->from('gym');

        $query = $this->db->get();

//        $row = $query->row();
//
//        return array(
//            'gym_phone_number' => ($row->phone_number),
//            'gym_address' => ($row->address));

        return $query->row();
    }

}

/* End of file user_model.php */
/* Location: ./application/models/contact_model.php */

<?php
class Purchase_model extends CI_Model {

	public function __construct()
	{
		// empty constructor
	}
	
	public function get_purchases_by_wallet_id( $id_wallet )
	{

		$this->db->select('*');
		$this->db->from('purchase');
		//$this->db->join('user', 'user.id_user = purchase.fk_id_user');
		$this->db->where('purchase.fk_id_wallet', $id_wallet);
		$this->db->order_by('purchase.date', 'desc');	

		$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
		
		// no data retrieved, some error had to occur
		return NULL; 
	}	
	
	public function get_purchases_and_user_data_by_wallet_id( $id_wallet )
	{

		$this->db->select('*');
		$this->db->from('purchase');
		$this->db->join('user', 'user.id_user = purchase.fk_id_user');
		$this->db->where('purchase.fk_id_wallet', $id_wallet);
		$this->db->order_by('purchase.date', 'desc');	

		$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
		
		// no data retrieved, some error had to occur
		return NULL; 
	}
	
	public function create_purchase( $purchase_date, $purchase_value, $id_service, $id_receiver_wallet )
	{
		$new_purchase = array(
			'date'  		=> $purchase_date ,
			'value'   		=> $purchase_value ,
			'fk_id_user'   	=> $id_service,
			'fk_id_wallet'	=> $id_receiver_wallet
			);

		$this->db->insert('purchase', $new_purchase); 
	}

}

/* End of file wallet_model.php */
/* Location: ./application/models/wallet_model.php */

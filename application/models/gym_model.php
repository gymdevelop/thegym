<?php
class Gym_model extends CI_Model {

	public function __construct()
	{	
		//$this->load->database();
		//parent::__construct();
	}
	
	public function get_gym_by_id( $id_gym = '1' )
	{
		
		$this->db->select('*');
		$this->db->from('gym');
		$this->db->where('id_gym', $id_gym);		

		$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		
		return NULL;
	}
	
	public function update_gym_by_id_and_data( $gym_id = 1, $data)
	{
		$this->db->where('id_gym', $gym_id);
		$this->db->update('gym', $data); 
	}
}

/* End of file gym_model.php */
/* Location: ./application/models/gym_model.php */

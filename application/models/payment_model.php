<?php
class Payment_model extends CI_Model {

	public function __construct()
	{
		// empty constructor
	}
	
	public function get_payments_by_wallet_id( $id_wallet )
	{

		$this->db->select('*');
		$this->db->from('payment');
		$this->db->join('payment_type', 'payment_type.id_payment_type = payment.fk_id_payment_type');
		$this->db->join('entry_permission', 'entry_permission.id_entry_permission = payment.fk_id_entry_permission');
		$this->db->where('payment.fk_id_wallet', $id_wallet);
		$this->db->order_by('payment.date', 'asc');	

		$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
		
		// no data retrieved, some error had to occur
		return NULL; 
	}

}

/* End of file wallet_model.php */
/* Location: ./application/models/wallet_model.php */

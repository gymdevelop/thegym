<HTML>
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>Prihlásenie</title>

<?php 
	$this->load->helper('html');
	echo link_tag('assets/css/assets/welcome_page.css');
	echo link_tag('favicon.ico', 'shortcut icon', 'image/ico');
?>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script>
		$(document).ready(function(){
				$("#login_view_wrapper").animate({ "top": "+=400px" }, "slow");
			});
	</script>	
</head>

<body>

<div id="login_view_wrapper">
<div id="contain_wrapper">

<div id="home_url_button_wrapper">
<?php echo anchor('common/welcome', '<img src="/gym/assets/css/assets/images/x.png"/>', array('title' => 'Späť na domovskú stránku', 'id' => 'home_url_link' )); ?>
</div>


<?php echo validation_errors(); ?>
<?php echo form_open('common/login'); ?>

	<div id="nick_email_phone_wrapper" class="login_wrappers">
		<div class="login_info_text">Prihlásiť sa do systému môžete zadaním Vášho nick-u, email-u alebo telefónneho čísla:</div>

		<!--label for="data">Nick\Email\Telefónne číslo</label-->

		<?php echo form_error('data'); ?>

		<input type="text" name="data" value="<?php echo set_value('data'); ?>" title="Váš nick, telefón alebo email" size="32" maxlength="64" />
	</div>

	<div id="password_wrapper"  class="login_wrappers">
		<div class="login_info_text">Autentifikáciu potvrdíte svojím heslom:</div>

		<?php echo form_error('password'); ?>
	
		<!--label for="password">Heslo</label-->
	
		<input type="password" id="password" name="password" value=""  title="Vaše heslo" size="32" maxlength="64" />
	</div>

	<div>
		<input type="submit" value="Odoslať" />
	</div>

</form>
</div> <!-- end of contain_wrapper -->
</div> <!-- end of login_view_wrapper -->





</body>
</html>

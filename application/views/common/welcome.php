<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Vitajte!</title>
	
	<?php 
	$this->load->helper('html');
	echo link_tag('assets/css/assets/jquery-ui-1.10.3.custom.css');
	echo link_tag('assets/css/assets/welcome_page.css');
	echo link_tag('favicon.ico', 'shortcut icon', 'image/ico');
	?>
	
	<script src="http://localhost/gym/assets/js/jquery-1.9.1.js" type="text/javascript" charset="utf-8"></script>
	<script src="http://localhost/gym/assets/js/jquery-ui-1.10.3.custom.js" type="text/javascript" charset="utf-8"></script>

	<script>
		$(document).ready(function () {
			$( ".jquery_button" ).button();
		});
	</script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
	</script>
	<script>
		$(document).ready(function(){
			$("#welcome_menu_login").click(function(){
				$("#welcome_menu_login").animate({ "top": "-=350px" }, "slow", function() {
						//thing to do when you animation is finished e.g.
						location.href = '/gym/index.php/common/login';
						}
					);
				});
				
			$("#welcome_menu_about_us").click(function(){
				$("#welcome_menu_login").animate({ "top": "-=350px" }, "slow" );
				$("#welcome_menu_about_us").animate({ "top": "-=350px" }, "slow", function() {
						//thing to do when you animation is finished e.g.
						location.href = '/gym/index.php/common/about_us';
						}
					);
				});
				
			$("#welcome_menu_gallery").click(function(){
				$("#welcome_menu_login").animate({ "top": "-=350px" }, "slow" );
				$("#welcome_menu_about_us").animate({ "top": "-=350px" }, "slow" );
				$("#welcome_menu_gallery").animate({ "top": "-=350px" }, "slow", function() {
						//TODO: change ! thing to do when you animation is finished e.g.
						location.href = '/gym/index.php/common/gallery';
						}
					);
				});

			$("#welcome_menu_price_list").click(function(){
				$("#welcome_menu_login").animate({ "top": "-=350px" }, "slow" );
				$("#welcome_menu_about_us").animate({ "top": "-=350px" }, "slow" );
				$("#welcome_menu_gallery").animate({ "top": "-=350px" }, "slow" );
				$("#welcome_menu_price_list").animate({ "top": "-=350px" }, "slow", function() {
						//thing to do when you animation is finished e.g.
						location.href = '/gym/index.php/common/price_list';
						}
					);
				});	

			$("#welcome_menu_contact").click(function(){
				$("#welcome_menu_login").animate({ "top": "-=350px" }, "slow" );
				$("#welcome_menu_about_us").animate({ "top": "-=350px" }, "slow" );
				$("#welcome_menu_gallery").animate({ "top": "-=350px" }, "slow" );
				$("#welcome_menu_price_list").animate({ "top": "-=350px" }, "slow" );
				$("#welcome_menu_contact").animate({ "top": "-=350px" }, "slow", function() {
						//thing to do when you animation is finished e.g.
						location.href = '/gym/index.php/common/contact';
						}
					);
				});	
				
			});
	</script>
</head>
<body>

<div id="welcome_menu">
	<div id="menu_wrapper">
		<div id="welcome_menu_login" class="welcome_menu_item">
		<!--button id="welcome_menu_login_button" class="jquery_button" title="Kliknite pre prihlásenie sa do systému">
			Prihlásenie
		</button -->
			<?php echo anchor('common/login', 'Prihlásenie', 'title="login"'); ?>
		</div>
		<div id="welcome_menu_about_us" class="welcome_menu_item">
			<?php echo anchor('common/about_us', 'O nás', 'title="Pár slov o nás"'); ?>
		</div>
		<div id="welcome_menu_gallery" class="welcome_menu_item">
			<?php echo anchor('common/gallery', 'Fotogaléria', 'title="Interiér i exteriér"'); ?>
		</div>
		<div id="welcome_menu_price_list" class="welcome_menu_item">
			<?php echo anchor('common/price_list', 'Cenník', 'title="cenník"'); ?>
		</div>
		<div id="welcome_menu_contact" class="welcome_menu_item">
<!--		<button id="welcome_menu_contact_button" class="jquery_button" title="Neváhajte nás kontaktovať">
			Kontakt
		</button>-->
				<?php echo anchor('common/contact', 'Kontakt', 'title="kontakt"'); ?>
		</div>
	</div>
</div>

<div id="container">
	<!-- h1>Vitajte na stránkach Vašej posilňovne!</h1 -->
</div>

</body>
</html>

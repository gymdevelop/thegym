<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
 <head>
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
  <title>Fotogaléria</title>
  
	<?php 
		echo link_tag('assets/css/assets/welcome_page.css');
		echo link_tag('assets/css/assets/gallery.css');
	?>
	<script src="http://localhost/gym/assets/js/jquery-1.9.1.js" type="text/javascript" charset="utf-8"></script>
	<script src="http://localhost/gym/assets/js/jquery.scrollTo.js" type="text/javascript" charset="utf-8"></script>

<script>

$(document).ready(function() {

	//Speed of the slideshow
	var speed = 5000;
	
	//You have to specify width and height in #slider CSS properties
	//After that, the following script will set the width and height accordingly
	$('#mask-gallery, #gallery li').width($('#slider').width());	
	$('#gallery').width($('#slider').width() * $('#gallery li').length);
	$('#mask-gallery, #gallery li, #mask-excerpt, #excerpt li').height($('#slider').height());
	
	//Assign a timer, so it will run periodically
	var run = setInterval('newsscoller(0)', speed);	
	
	$('#gallery li:first, #excerpt li:first').addClass('selected');

	//Pause the slidershow with clearInterval
	$('#btn-pause').click(function () {
		clearInterval(run);
		return false;
	});

	//Continue the slideshow with setInterval
	$('#btn-play').click(function () {
		run = setInterval('newsscoller(0)', speed);	
		return false;
	});
	
	//Next Slide by calling the function
	$('#btn-next').click(function () {
		newsscoller(0);	
		return false;
	});	

	//Previous slide by passing prev=1
	$('#btn-prev').click(function () {
		newsscoller(1);	
		return false;
	});	
	
	//Mouse over, pause it, on mouse out, resume the slider show
	$('#slider').hover(
	
		function() {
			clearInterval(run);
		}, 
		function() {
			run = setInterval('newsscoller(0)', speed);	
		}
	); 	
	
});


function newsscoller(prev) {

	//Get the current selected item (with selected class), if none was found, get the first item
	var current_image = $('#gallery li.selected').length ? $('#gallery li.selected') : $('#gallery li:first');
	var current_excerpt = $('#excerpt li.selected').length ? $('#excerpt li.selected') : $('#excerpt li:first');

	//if prev is set to 1 (previous item)
	if (prev) {
		
		//Get previous sibling
		var next_image = (current_image.prev().length) ? current_image.prev() : $('#gallery li:last');
		var next_excerpt = (current_excerpt.prev().length) ? current_excerpt.prev() : $('#excerpt li:last');
	
	//if prev is set to 0 (next item)
	} else {
		
		//Get next sibling
		var next_image = (current_image.next().length) ? current_image.next() : $('#gallery li:first');
		var next_excerpt = (current_excerpt.next().length) ? current_excerpt.next() : $('#excerpt li:first');
	}

	//clear the selected class
	$('#excerpt li, #gallery li').removeClass('selected');
	
	//reassign the selected class to current items
	next_image.addClass('selected');
	next_excerpt.addClass('selected');

	//Scroll the items
	$('#mask-gallery').scrollTo(next_image, 600);		
	$('#mask-excerpt').scrollTo(next_excerpt, 600);					
	
}
</script>

 </head>
 <body>

<div id="debug"></div>

<div id="wrapper">

<div id="slider">
	<div id="mask-gallery">
	<ul id="gallery">
		<li><img src="/gym/assets/css/assets/images/gallery_images/gym_1.jpg" width="500" height="400" alt=""/></li>
		<li><img src="/gym/assets/css/assets/images/gallery_images/gym_2.jpg" width="500" height="400" alt=""/></li>
		<li><img src="/gym/assets/css/assets/images/gallery_images/gym_3.jpg" width="500" height="400" alt=""/></li>
	</ul>
	</div>
	
	<div id="mask-excerpt">
	<ul id="excerpt">
		<li>Väčšinu strojov predstavujú produkty firmy Life Fitness.</li>
		<li>Zákazník má k dispozícii široký výber jednoručiek počínajúc váhou 5 libier až po 150 libier.</li>
		<li>Vetraniu v miestnosti napomáha aj systém 3 dômyselne umiestnených rotačných ventilátorov.</li>
	</ul>
	</div>

</div>

<div id="buttons">
	<a href="#" id="btn-prev"><img src="/gym/assets/css/assets/images/gallery_css/previous.png"  width="40" height="40" alt="Previous"/></a> 
	<a href="#" id="btn-pause"><img src="/gym/assets/css/assets/images/gallery_css/pause.png"  width="50" height="50" alt="Pause"/></a> 
	<a href="#" id="btn-play"><img src="/gym/assets/css/assets/images/gallery_css/play.png"  width="50" height="50" alt="Play"/></a> 
	<a href="#" id="btn-next"><img src="/gym/assets/css/assets/images/gallery_css/next.png"  width="40" height="40" alt="Next"/></a>
</div>

</div>

<div class="clear"></div>


 </body>
</html>

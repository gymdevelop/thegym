<html>
    <head>
        <meta http-equiv="Content-Type"
              content="text/html;charset=UTF-8">
        <title>Kontakt</title>
        <?php
        $this->load->helper('html');
        $this->load->helper('form');
        echo link_tag('assets/css/assets/contact_page.css');
        ?>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script>
            $(document).ready(function() {
                $("#contact_view_wrapper").animate({"top": "+=400px"}, "slow");
            });
        </script>
    </head>
    <body>
        <div id="contact_view_wrapper">
            <div id="contain_wrapper">

                <div id="home_url_button_wrapper">
                    <?php echo anchor('common/welcome', '<img src="/gym/assets/css/assets/images/x.png"/>', array('title' => 'Späť na domovskú stránku', 'id' => 'home_url_link')); ?>
                </div>
                <?php echo '<div id="contac_us_label">Kontakt</div>' ?>
                <div id="contact_data">
                    <div class="contact_data_phone_number">

                        <?php
                        echo form_label("Tel. cislo: ", "phone_number");
                        echo br();
                        ?>

                    </div>
                    <div class="contact_data_address_value">
                        <?php echo $gym_phone_number;
                        ?>
                    </div>
                    <div class="contact_data_address">

                        <?php
                        echo form_label("Adresa: ", "address");
                        echo br();
                        ?>

                    </div>
                    <div class="contact_data_phone_number_value">
                        <?php echo $gym_address;
                        ?>
                    </div>

                </div>
                <!--formular -->
                <div id="contact_form">
                    <div id="online_form">
                        <?php echo 'Online formular'; ?>
                    </div>
                    <?php
                    //ci neje predmet a sprava porozna
                    echo validation_errors();

                    echo form_open("common/contact/send_email");
                    ?>
                    <div class="left_form">
                        <?php
                        echo $message;
                        echo br();
                        //name
                        echo '<div id="name">';
                        echo form_label("Name: ", "name");
                        echo br();
                        $data = array(
                            "name" => "name",
                            "id" => "name",
                            "value" => set_value("name")
                        );
                        echo form_input($data);
                        echo '</div>';
                        
                        //subject
                        echo '<div id="subject">';
                        echo form_label("Predmet: ", "subject");
                        echo br();
                        $data = array(
                            "name" => "subject",
                            "id" => "subject",
                            "value" => set_value("subject")
                        );
                        echo form_input($data);
                        echo '</div>';

                        //email
                        echo '<div id="email">';
                        echo form_label("Email: ", "email");
                        echo br();
                        $data = array(
                            "name" => "email",
                            "id" => "email",
                            "value" => set_value("email")
                        );
                        echo form_input($data);
                        echo '</div>';
                        
                        ?>
                    </div>
                    <div class="right_form">
                        <?php
                        //message
                        echo '<div id="message">';
                        echo form_label("Sprava: ", "message");
                        echo br();
                        $data = array(
                            "name" => "message",
                            "id" => "message",
                            "value" => set_value("message"),
                            'rows' => '8',
                            'cols' => '30'
                        );
                        echo form_textarea($data);
                        echo '</div>';
                        ?>
                    </div>
                    <div class="submit_button">
                        <?php
                        echo '<div id="submit_button">';
                        echo form_submit("contact_submit_button", "Odoslat");
                        echo '</div>';
                        ?>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div> <!-- end of contain_wrapper -->
        </div> <!-- end of login_view_wrapper -->

    </body>
</html>

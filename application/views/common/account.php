	<?php 
	// helpers
		$this->load->helper('html');
		$this->load->helper('url');
		
	// css
	echo link_tag('assets/css/assets/admin_stylesheet.css');
	?>

<?php echo validation_errors(); ?>
<?php echo form_open('common/account/edit'); ?>

<div class="form_wrapper">

<h1>Vaše údaje:</h1>

<label for="nick">
<span>Nick</span>
<input type="text" id="nick" name="nick" value="<?php echo set_value('nick', $nick); ?>" maxlength="32" size="32" />
<?php echo form_error('nick'); ?>
</label>

<label for="email">
<span>Email</span>
<input type="text" id="email" name="email" value="<?php echo set_value('email', $email); ?>" maxlength="64" size="32" />
<?php echo form_error('email'); ?>
</label>

<label for="phone">
<span>Telefónne číslo</span>
<input type="text" id="phone" name="phone" value="<?php echo set_value('phone', $phone ); ?>" maxlength="32" size="32" />
<?php echo form_error('phone'); ?>
</label>

<label for="firstname">
<span>Krstné meno</span>
<input type="text" id="firstname" name="firstname" value="<?php echo set_value('firstname', $firstname ); ?>" maxlength="32" size="32" />
<?php echo form_error('firstname'); ?>
</label>

<label for="lastname">
<span>Priezvisko</span>
<input type="text" id="lastname" name="lastname" value="<?php echo set_value('lastname', $lastname ); ?>" maxlength="32" size="32" />
<?php echo form_error('lastname'); ?>
</label>


<label for="gender">
<span>Pohlavie</span>
<input type="radio"  id="gender" name="gender" value="0" <?php echo (($gender == 0) ? 'checked="true"' : '') ; ?> /><span class="spec">Muž</span>
<input type="radio"  id="gender" name="gender" value="1" <?php echo (($gender == 1) ? 'checked="true"' : '') ; ?> /><span class="spec">Žena</span>
<?php echo form_error('gender'); ?>
</label>

<div class="msg">
Ďalšie 3 kolónky vyplňte, len ak chcete zmeniť heslo samotné. <br />Na zmenu ostatných údajov zadávať heslo nemusíte.
</div>

<label for="old_password">
<span>Staré heslo</span>
<input type="password" id="old_password"name="old_password" value="" maxlength="64" size="32" />
<?php echo form_error('old_password'); ?>
</label>

<label for="new_password">
<span>Nové heslo</span>
<input type="password" id="new_password" name="new_password" value="" maxlength="64" size="32" />
<?php echo form_error('new_password'); ?>
</label>

<label for="new_password_conf">
<span>Zopakujte nové heslo</span>
<input type="password" id="new_password_conf" name="new_password_conf" value="" maxlength="64" size="32" />
<?php echo form_error('new_password_conf'); ?>
</label>

<input type="submit" class="button" value="Odoslať" />

</form>

<?php if( $is_global_inf ) echo '<div id="global_err">'. $global_inf_msg . '</div>' ?>

</div>


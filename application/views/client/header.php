<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>Vitaj</title>
	
	<?php 
		$this->load->helper('html');
		$this->load->helper('url');
		echo link_tag('favicon.ico', 'shortcut icon', 'image/ico');
	?>	
	
</head>
<body>

<div id="menu_admin">
	<div id="main" >
		<?php echo anchor('common/welcome', 'Hlavná stránka', 'title="main"'); ?>
	</div>
	<div id="wallet" >
		<?php echo anchor('client/wallet', 'Peňaženka', 'title="wallet"'); ?>
	</div>
	<div id="purchases" >
		<?php echo anchor('client/purchase', 'Dobitia', 'title="credit purchases"'); ?>
	</div>
	<div id="payments" >
		<?php echo anchor('client/payment', 'Platby', 'title="payments"'); ?>
	</div>
	<div id="account" >
		<?php echo anchor('common/account', 'Účet', 'title="account"'); ?>
	</div>	
	<div id="logout" >
		<?php echo anchor('home/logout', 'Odhlásenie', 'title="logout"'); ?>
	</div>
</div>

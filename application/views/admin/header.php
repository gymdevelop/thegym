<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <title>Vitaj</title>

        <?php
        $this->load->helper('html');
        $this->load->helper('url');
        echo link_tag('favicon.ico', 'shortcut icon', 'image/ico');
        ?>	

    </head>
    <body>

        <div id="menu_admin">
            <div id="main" >
                <?php echo anchor('common/welcome', 'Hlavná stránka', 'title="main"'); ?>
            </div>
            <div id="logout" >
                <?php echo anchor('home/logout', 'Odhlásenie', 'title="logout"'); ?>
            </div>
        </div>

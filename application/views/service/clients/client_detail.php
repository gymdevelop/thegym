<h3> Detail zákazníka: </h3>

<div id="client"> 
	<div id="client_account_table">
		<?php echo $client_account_data_table  ?>
	</div>
	<div id="wallet_section">
		<div id="credit_info">
			<p> Aktuálny kredit je <?php echo $client_actual_credit ?>. </p>
			<span id="entry_permission_image" style="margin: 10px 10px 10px 10px;">
				<?php if( FALSE )
					{
						$src = "yes.png";
						$alt = "Yes!";
					} else {
						$src = "no.png";
						$alt = "No!";
					};
					echo '<img src="/gym/assets/css/assets/images/common_images/' . $src . '" alt="'. $alt . '"/>';
				?>
			</span>
		</div>
		<div id="purchases">
			<h5> Dobiť zákazníkovi kredit: </h5>
			
			<?php echo form_open('service/clients_handler/create_purchase'); ?>
			<p>Hondota:</p>
			<input type="number" name="value" value="0" />
			
			<p>Dátum dobitia:</p>
			<input type="date" value="<?php echo $date_today ?>" name="purchase_date" readonly="readonly" />
			
			<p>Prijíma:</p>
			<input type="text" value="<?php echo $receiver ?>" name="receiver" readonly="readonly" />
			
			<?php echo form_hidden('id_wallet', $id_wallet ); ?>
			
			<div><input type="submit" value="Dobiť kredit" /></div>
			
			</form>
		</div>
	</div>
	<div>
	
	</div>
</div>

	<?php 
	// helpers
		$this->load->helper('html');
		$this->load->helper('url');
		
	// css
	echo link_tag('assets/css/assets/admin_stylesheet.css');
	?>

<?php echo form_open('service/gym_handler/change_data'); ?>

<div class="form_wrapper">	
	
	<h1>Údaje posilňovne:</h1>
	
	<label for="name">
	<span>Názov</span>	
	<input type="text" id="name" name="name" value="<?php echo $name ?>" />
	</label>

	<label for="description">
	<span>Popis</span>	
	<textarea id="description" class="message" name="description" rows="4" cols="50"><?php echo $description ?></textarea>
	</label>
			
	<label for="phone">
	<span>Telefónne číslo</span>		
	<input type="text" id="phone" name="phone" value="<?php echo $phone_number ?>" />
	</label>
	
	<label for="address">
	<span>Adresa</span>		
	<input type="text" id="address" name="address" value="<?php echo $address ?>" />
	</label>	
	
	<label for="price_list">
	<span>Cenník</span>	
	<textarea name="price_list" class="message" id="price_list" rows="4" cols="50"><?php echo $price_list ?></textarea>
	</label>
			
	<?php echo form_hidden('id_gym', 1 ); ?>
			
	<div><input type="submit" class="button" value="Zmeniť údaje" /></div>

</div>

</form>

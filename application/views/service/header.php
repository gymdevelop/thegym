<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>Vitaj</title>
	
	<?php 
	// helpers
		$this->load->helper('html');
		$this->load->helper('url');
	
	//favicon	
	echo link_tag('favicon.ico', 'shortcut icon', 'image/ico');
		
	// css
	echo link_tag('assets/css/assets/admin_menu.css');
	?>
	
	<script src="http://localhost/gym/assets/js/jquery-1.9.1.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="http://localhost/gym/assets/js/jquery-easing-1.3.pack.js"></script>
	<script type="text/javascript" src="http://localhost/gym/assets/js/jquery-easing-compatibility.1.2.pack.js"></script>	
	
	<script type="text/javascript">
	$(document).ready(function () {

	// find the elements to be eased and hook the hover event
	$('div.jimgMenu ul li a').hover(function() {

		// if the element is currently being animated (to a easeOut)...
		if ($(this).is(':animated')) {
			$(this).stop().animate({width: "310px"}, {duration: 450, easing:"easeOutQuad"});
		} else {
			// ease in quickly
			$(this).stop().animate({width: "310px"}, {duration: 400, easing:"easeOutQuad"});
		}
	}, function () {
			
			// on hovering out, ease the element out
			if ($(this).is(':animated')) {
				$(this).stop().animate({width: "78px"}, {duration: 400, easing:"easeInOutQuad"})
			} else {
				// ease out slowly
				$(this).stop('animated:').animate({width: "78px"}, {duration: 450, easing:"easeInOutQuad"});
			}
		});
	});
</script>	
	
</head>
<body>

<div class="jimgMenu">
  <ul>
    <li class="landscapes"><?php echo anchor('common/welcome', 'Hlavná stránka', 'title="Hlavná stránka"'); ?></li>
    <li class="people"><?php echo anchor('service/clients_handler', 'Zákazníci', 'title="Zákazníci"'); ?></li>
    <li class="nature"><?php echo anchor('service/gym_handler', 'Posilňovňa', 'title="Posilňovňa"'); ?></li>
    <li class="abstract"><?php echo anchor('common/account', 'Účet', 'title="Nastavenia účtu"'); ?></li>
    <li class="urban"><?php echo anchor('home/logout', 'Odhlásenie', 'title="Odhlásenie"'); ?></li>
  </ul>
</div>

<div id="container">
<div id="content">

<!--div id="menu_admin">
	<div id="main" >
		<?/*php echo anchor('common/welcome', 'Hlavná stránka', 'title="main"'); ?>
	</div>
	<div id="clients" >
		<?php echo anchor('service/clients_handler', 'Zákazníci', 'title="clients"'); ?>
	</div>	
	<div id="gym" >
		<?php echo anchor('service/gym_handler', 'Posilňovňa', 'title="The gym"'); ?>
	</div>	
	<div id="account" >
		<?php echo anchor('common/account', 'Účet', 'title="account"'); ?>
	</div>	
	<div id="logout" >
		<?php echo anchor('home/logout', 'Odhlásenie', 'title="logout"'); */?>
	</div>
</div-->

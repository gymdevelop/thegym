<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

session_start();

class MY_Controller extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
	}

	public function authentify_any()
	{
		if( $this->session->userdata('actual_user') ){
			
			return TRUE;
		}
		
		//If no session, redirect to login page
		redirect('common/login', 'refresh');
			
		return FALSE;
	}
	
	public function authentify_service()
	{		
		if( $this->session->userdata('actual_user') ){
				
			$session_data = $this->session->userdata('actual_user');

			$user_group = $this->user_model->get_user_group_name_and_supreme_flag( $session_data['id_user'] );
			
			if ( $user_group['group_name'] == 'service' )
			{			
				return TRUE;
			}else
			{
				//If no session, redirect to login page
				redirect('common/login', 'refresh');				
				
				return FALSE;
			}
		}
		
		//If no session, redirect to login page
		redirect('common/login', 'refresh');
			
		return FALSE;
	}	
	
	function logout()
	{
		$this->session->unset_userdata('actual_user');
		session_destroy();
		redirect('home', 'refresh');
	}	
}


/* End of file Commander.php */
/* Location: ./application/libraries/Commander.php */

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Price_list extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('gym_model');
    }

    function index() {
        $row = $this->gym_model->get_gym_by_id(); // get_gym_by_id(1);

        $data['gym_price_list'] = $row->price_list;
        
        $this->load->view('common/price_list', $data);
    }
}

/* End of file Price_list.php */
/* Location: ./application/controllers/common/Price_list.php */

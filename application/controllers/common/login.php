<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {

	private $form_data_array = array(
		'nick' => array(), 
		'email' => array(),
		'phone' => array()
		);
	
	// rendering this view is reaction for all incorrect login attempts
	private $login_view 		= 'common/login_view';
	
	// main application controller that handles (any) user after successful log on
	private $home_app_controller = 'home';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
	}

	function index()
	{
		// at first check if user is not already logged in
		if( $this->session->userdata('actual_user') ){
			
			redirect( $this->home_app_controller , 'refresh');
			return ;
		}
		
		$this->load->helper('form');
		$this->load->library('form_validation');

		// setting validation for nick
		$this->form_validation->set_rules('data', 'Data', 'trim|required|max_length[64]|callback_persistance_check');
		// and password
		$this->form_validation->set_rules('password', 'Password', 'trim|max_length[32]');
		
		// validate
		$validation_result = $this->form_validation->run();
		
		if ( $validation_result == FALSE )
		{
			log_message('debug','First render or validation has not been successful');
			
			$this->load->view( $this->login_view );
			return ;
		}
		
		
		// try to authentify user with entered data
		$authentified_user = $this->_authentify( $this->form_data_array, $this->input->post('password') );
		
		// check result of authetification
		if( $authentified_user == NULL )
		{
			// valid inputs, but authentification failed	
			
			log_message('debug', 'User has not went through the authentification.');

			$this->form_validation->set_message('password_save', 'Zadané heslo nie je správne.');

			// render the same view
			$this->load->view( $this->login_view );
			
			return ;
		}

		// login successful
		
		log_message('debug','Authentification successful, let`s retrieve user group and supreme flag...');
		
		// retrieve user's supreme_flag
		$array_group_name_sup_flag = $this->user_model->get_user_group_name_and_supreme_flag( $authentified_user['id_user'] );
				
		if( is_null($array_group_name_sup_flag) )
		{
			// sth went wrong, how is possible that supreme_flag and user_group's name cannot be found?
			
			//log
			log_message('debug', 'User "' . $authentified_user['nick'] . '" NOT actually logged in on because his/her user_group.name and/or user_group.supreme_flag could NOT be found.');

			// do not set any (SESSION) data

			// just redirect back to login TODO: set error message later
			$this->load->view( $this->login_view );
			
			return ;
			
		}

		//	*** finally successful login ! :) ***
		
		 $date_now = time();
		
		// update user's last login timestamp
		$this->user_model->update_last_login( $authentified_user['id_user'] , $date_now );
			
		// create and set SESSION data for further needs
			
		$session_array = array();
		$session_array = array(
			'id_user'       => $authentified_user['id_user'],
			'nick'          => $authentified_user['nick'],
			'user_group'    => $array_group_name_sup_flag['group_name'],
			'supreme_flag'	=> $array_group_name_sup_flag['supreme_flag']
			);
		
		$this->session->set_userdata('actual_user', $session_array);
					
		// log
		log_message(
			'debug',
			'User "' . $authentified_user['nick'] . '" logged in on ' . date('d.m.Y H:i:s', $date_now) . ' as ' . $array_group_name_sup_flag['group_name'] . '.'
			);
			
		// note: controller `home` takes care of rendering correct view, here Login controller just handles authentification
		// and validation, that's why SWITCH clause has been moved to common/home controller ;)
		redirect( $this->home_app_controller , 'refresh');
	}
	
	/**
	 * Returns NULL if there is no such a user in database or loaded user if successful. 
	 */
	private function _authentify( $form_data_array, $password_value )
	{
		
		$result = NULL;
		
		foreach( $form_data_array as $single_row ){
			// if valid input, so any persistance has been found try to authentify
			if( !empty( $single_row['is_persistent'] ) && !is_null($single_row['is_persistent']) && $single_row['is_persistent'] == TRUE)
			{
				//try to perform authentification
				$result = $this->user_model->authentify( $single_row['column_name'], $single_row['data'], $password_value );
				
				if ( $result != NULL )
				{
					return $result;
				}
				// else continue
			}
		}
		
		return $result;
	}
	
	/**
	 * Sets data that run through validation in order to authentification attempts later.
	 */
	public function persistance_check( $data ){
		
		$at_least_one_correct = FALSE;
		
		if( $this->user_model->is_persistant_by_column_and_value('nick', $data ) )
		{
			$at_least_one_correct = TRUE;
			
			$this->form_data_array['nick'] = array(
				'column_name' => 'nick',
				'is_persistent' => TRUE,
				'data' => $data
				);
		}
		
		if( $this->user_model->is_persistant_by_column_and_value('email', $data) )
		{
			$at_least_one_correct = TRUE;
			
			$this->form_data_array['email'] = array(
				'column_name' => 'email',
				'is_persistent' => TRUE,
				'data' => $data
				);
		}//else{...}
		
		if( $this->user_model->is_persistant_by_column_and_value('phone', $data) )
		{
			$at_least_one_correct = TRUE;
			
			$this->form_data_array['phone'] = array(
				'column_name' => 'phone',
				'is_persistent' => TRUE,
				'data' => $data
				);
		}

		return $at_least_one_correct;
	}
}

/* End of file login.php */
/* Location: ./application/controllers/common/login.php */

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('contact_model');
    }

    function index() {
        $row = $this->contact_model->get_gym_phone_number_and_address();

        $data['message'] = "";
        $data['gym_phone_number'] = $row->phone_number;
        $data['gym_address'] = $row->address;

//        $data['gym_phone_number'] = $this->contact_model->get_gym_phone_number_and_address()->gym_phone_number;
//        $data['gym_address]'] = $this->contact_model->get_gym_phone_number_and_address()->gym_address;
        //$data['query'] = $this->contact_model->get_gym_phone_number_and_address();
        $this->load->view('common/contact', $data);
    }

    public function send_email() {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("subject", "Predmet", "required|xss_clean");
        $this->form_validation->set_rules("message", "Sprava", "required|xss_clean");
        $this->form_validation->set_rules("name", "Name", "required|xss_clean");
        $this->form_validation->set_rules("email", "Email", "required|xss_clean");

        if ($this->form_validation->run() == FALSE) {
            $row = $this->contact_model->get_gym_phone_number_and_address();
            $data['message'] = "";
            $data['gym_phone_number'] = $row->phone_number;
            $data['gym_address'] = $row->address;
            $this->load->view('common/contact', $data); //toto len tak pre paradu
        } else {
            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => 'miter.malinak@gmail.com',
                'smtp_pass' => 'mikroskop6490',
                'mailtype' => 'html',
                'charset' => 'iso-8859-1'
            );
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");

            $this->email->from(set_value("email"),  set_value('name'));
            $this->email->to("miter.malinak@gmail.com");
            $this->email->subject(set_value("subject"));
            $this->email->message(set_value("message"));

            $this->email->send();

            echo $this->email->print_debugger();

            $row = $this->contact_model->get_gym_phone_number_and_address();
            $data['message'] = "Email bol uspesne odoslany!";
            $data['gym_phone_number'] = $row->phone_number;
            $data['gym_address'] = $row->address;
            $this->load->view('common/contact', $data);
        }
    }

}

/* End of file Contact.php */
/* Location: ./application/controllers/common/Contact.php */

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    function index() {
        //$row = $this->gym_model->get_gym_by_id(); // get_gym_by_id(1);

        //$data['gym_price_list'] = $row->price_list;
        
        $this->load->view('common/gallery');
    }
}

/* End of file Gallery.php */
/* Location: ./application/controllers/common/Gallery.php */

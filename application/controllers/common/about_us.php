<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class About_us extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('gym_model');
    }

    function index() {
        $row = $this->gym_model->get_gym_by_id(); // get_gym_by_id(1);

        $data['gym_description'] = $row->description;
        
        $this->load->view('common/about_us', $data);
    }
}

/* End of file contact.php */
/* Location: ./application/controllers/common/contact.php */

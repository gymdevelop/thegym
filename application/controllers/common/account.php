<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('user_model');
	}

	function index()
	{
		// check if user logged
		if ( ! $this->authentify_any() ){ return ; }
		
		$this->load->helper(array('form'));
		
		// just render, no need to show any special message
		$data['is_global_inf'] = FALSE;
		
		$session_data = $this->session->userdata('actual_user');

		$user 		= $this->user_model->get_user_by_id( $session_data['id_user'] );
		$user_group = $this->user_model->get_user_group_name_and_supreme_flag( $session_data['id_user'] );
		
		log_message('debug', 'Account.index() user: ' . $user['nick'] . ' ... ' . $user['gender']);
		
		$data['nick'] 			= $user['nick'];
		$data['firstname']  	= $user['firstname'];
		$data['lastname'] 		= $user['lastname'];
		$data['phone'] 			= $user['phone'];
		$data['email'] 			= $user['email'];
		$data['gender'] 		= $user['gender'];
		
		$this->load->view( $user_group['group_name'] . '/header', $data);
		$this->load->view('common/account', $data);
		$this->load->view($user_group['group_name'] . '/footer', $data);
	}
	
	function edit()
	{
		// check if user logged
		if ( ! $this->authentify_any() ){ return ; }
		
		$this->load->helper(array('form'));
		$this->load->library('form_validation');
		
		$is_global_inf 				= FALSE;
		$global_inf_msg				= '';
		$atributes_to_be_edited 	= array();
		$validation_result      	= FALSE;
		$are_passwords_identical	= TRUE;
		$sql_update_user_query 		= 'UPDATE user SET ';
		$sql_is_update_required		= FALSE;
		
		$session_data = $this->session->userdata('actual_user');
		$user_persisted = $this->user_model->get_user_by_id( $session_data['id_user'] );
		$user_group = $this->user_model->get_user_group_name_and_supreme_flag( $session_data['id_user'] );
		
		
		// checking changes and setting validation rules
		if ( $user_persisted['nick'] !=  $this->input->post('nick')){
			
			// trying to change a nick
			
			// set validation rules
			$this->form_validation->set_rules('nick', 'Nick', 'trim|required|min_length[2]|max_length[32]|callback_nick_check');
			
			$sql_update_user_query .= 'nick = "' .  $this->input->post('nick') . '"'; // ' nick = "new_nick"'
			
			// set SQL execution required flag
			$sql_is_update_required = TRUE;
		}
		
		if ( $user_persisted['firstname'] !=  $this->input->post('firstname')){
			
			// trying to change a firstname
			
			// set validation rules
			$this->form_validation->set_rules('firstname', 'Firstname', 'trim|required|min_length[2]|max_length[32]');
			
			$this->_add_comma_if_not_first_att_set( $sql_is_update_required, $sql_update_user_query );
			
			$sql_update_user_query .= ' firstname = ' .  $this->db->escape($this->input->post('firstname')) ; // ' firstname = "new_firstname"'
		}
		
		if ( $user_persisted['lastname'] !=  $this->input->post('lastname')){
			
			// trying to change a lastname
			
			// set validation rules
			$this->form_validation->set_rules('lastname', 'Lastname', 'trim|required|min_length[2]|max_length[32]');
			
			$this->_add_comma_if_not_first_att_set( $sql_is_update_required, $sql_update_user_query );
			
			$sql_update_user_query .= ' lastname = ' .  $this->db->escape( $this->input->post('lastname') ) ; // ' lastname = "new_lastname"'
		}		

		if ( $user_persisted['phone'] !=  $this->input->post('phone')){
			
			// trying to change a phone
			
			// set validation rules
			$this->form_validation->set_rules('phone', 'Phone', 'trim|required|min_length[2]|max_length[32]|callback_phone_check');// TODO add 
			
			$this->_add_comma_if_not_first_att_set( $sql_is_update_required, $sql_update_user_query );
			
			$sql_update_user_query .= ' phone = ' .  $this->db->escape( $this->input->post('phone') ) ; // ' phone = "new_phone"'
		}
		
		if ( $user_persisted['email'] !=  $this->input->post('email')){
			
			// trying to change a phone
			
			// set validation rules
			$this->form_validation->set_rules('email', 'Email', 'trim|required|min_length[2]|max_length[32]|callback_email_check');// TODO add 
			
			$this->_add_comma_if_not_first_att_set( $sql_is_update_required, $sql_update_user_query );
			
			$sql_update_user_query .= ' email = ' .  $this->db->escape( $this->input->post('email')) ; // ' email = "new_email"'
		}
		
		if ( $user_persisted['gender'] !=  $this->input->post('gender')){
			
			// trying to change a gender
			
			// set validation rules
			$this->form_validation->set_rules('gender', 'Gender', 'required');
			
			$this->_add_comma_if_not_first_att_set( $sql_is_update_required, $sql_update_user_query );
			
			$sql_update_user_query .= ' gender = ' .  $this->db->escape( $this->input->post('gender') ) ; // ' gender = new_gender' //number
		}
		
		if ( $user_persisted['password'] ==  $this->input->post('old_password')){
			
			// trying to change a password
			
			// set validation rules
			$this->form_validation->set_rules('new_password', 'Password', 'trim|required|min_length[4]|max_length[64]');
			$this->form_validation->set_rules('new_password_conf', 'Password', 'trim|required|min_length[4]|max_length[64]');
			
			// check if new password and confirmation password are the same
			if ( $this->input->post('new_password') != $this->input->post('new_password_conf') )
			{
				$are_passwords_identical = FALSE;
			}
			
			$this->_add_comma_if_not_first_att_set( $sql_is_update_required, $sql_update_user_query );
			
			$sql_update_user_query .= 'password = "' .  $this->input->post('new_password') . '"'; // ' password = "new_password"'
			
			// set SQL execution required flag
			$sql_is_update_required = TRUE;
		}		
		
		// check if anything has changed before validation or query execution
		if ( ! $sql_is_update_required )
		{
			
			$data['is_global_inf'] = TRUE;
			$data['global_inf_msg'] = 'Neboli zadané žiadne nové dáta na zmenu. Všetky položky účtu ostávajú nezmenené.';
			
			$this->_set_user_data( $data );			
			
			$this->load->view( $user_group['group_name'] . '/header' , $data);
			$this->load->view('common/account', $data);
			$this->load->view( $user_group['group_name'] . '/footer', $data);
			
			return ;
		}
		
		// validate
		$validation_result = $this->form_validation->run();
		
		if ( $validation_result == FALSE OR $are_passwords_identical == FALSE)
		{
			// validation failed
			
			// log message
			log_message('debug','User tried to change data, validation unsuccessful or passwords are different.');
			
			//inform user
			
			$data['is_global_inf'] = TRUE;
			$data['global_inf_msg'] = 'Validácia dát neprebehla úspešne.';
			
			if ( $are_passwords_identical == FALSE )
			{
				$data['global_inf_msg'] .= ' Potvrdenie nového hesla nie je úspešné, heslá sa navzájom líšia.';
			}
			
			// unset sql query
			$sql_update_user_query  = '';
			$sql_is_update_required = FALSE;
			
			// set data
			$this->_set_user_data( $data );
			
			$this->load->view( $user_group['group_name'] . '/header' , $data);
			$this->load->view('common/account', $data);
			$this->load->view( $user_group['group_name'] . '/footer', $data);
			
			return ;
		}
		
		// all validation OK
		
		// log and execute query
		$sql_update_user_query .= ' WHERE id_user = ' .  $session_data['id_user'] . ';' ;
				
		log_message('debug','SQL query before execution:' . $sql_update_user_query );
		
		$sql_update_user_result = $this->user_model->execute_query( $sql_update_user_query );
		
		// unset sql query
		//$sql_update_user_query  = '';
		//$sql_is_update_required = FALSE;			
		
		$this->_set_user_data( $data );
		
		// check result
		if ( $sql_update_user_result == TRUE )
		{
			// success
			$this->load->view( $user_group['group_name'] . '/header' , $data);
			$this->load->view('common/account_edited_succ', $data);
			$this->load->view( $user_group['group_name'] . '/footer', $data);	
			
			return ;
		}else
		{
			// fail

			$this->load->view( $user_group['group_name'] . '/header' , $data);
			$this->load->view('common/account', $data);
			$this->load->view( $user_group['group_name'] . '/footer', $data);
			
			return;
		}
	}
	
	public function nick_check(){
		
		return TRUE;
	}
	
	public function phone_check(){
		
		return TRUE;
	}	
	
	public function email_check(){
		
		return TRUE;
	}
	
	private function _add_comma_if_not_first_att_set( &$p_sql_is_update_required, &$p_sql_update_user_query )
	{
		
		if ( $p_sql_is_update_required == TRUE ) 
		{
			$p_sql_update_user_query .= ',';
		}else
		{
			$p_sql_is_update_required = TRUE;
		}
	}

	private function _set_user_data( &$data )
	{
		// set data
		$data['nick'] 			= $this->input->post('nick');
		$data['firstname']  	= $this->input->post('firstname');
		$data['lastname'] 		= $this->input->post('lastname');
		$data['phone'] 			= $this->input->post('phone');
		$data['email'] 			= $this->input->post('email');
		$data['gender'] 		= $this->input->post('gender');
	}

}

/* End of file wallet.php */
/* Location: ./application/controllers/client/wallet.php */

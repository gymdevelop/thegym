<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI

class Home extends MY_Controller {	//CI_Controller {

		public function __construct()
		{
			parent::__construct();
		}

		function index()
		{
	
			if ( ! $this->authentify_any() )// funguje aj takto: !parent::authentify() )
			{
				return;
			}
	
			$session_data = $this->session->userdata('actual_user');
     
			$data['id_user'] = $session_data['id_user'];
			$data['nick']    = $session_data['nick'];
			
			switch( $session_data['user_group'] ){
				case 'client' :
					$this->load->view('client/header', $data);
					$this->load->view('common/home_view', $data);
					$this->load->view('client/footer', $data);
					break;
				case 'service' :
					$this->load->view('service/header', $data);
					$this->load->view('common/home_view', $data);
					$this->load->view('service/footer', $data);
					break;
				case 'admin' :
					$this->load->view('admin/header', $data);
					$this->load->view('common/home_view', $data);
					$this->load->view('admin/footer', $data);
					break;
				default:
					redirect('common/login', 'refresh');
					break;
			}
		}
	}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clients_handler extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('user_model');
		$this->load->model('wallet_model');
		$this->load->model('payment_model');
		$this->load->model('purchase_model');
		
		$this->load->helper(array('form','html'));
		
		$this->load->library('table');
		$this->load->library('form_validation');
	}

	function index()
	{
		// check if user logged
		if ( ! $this->authentify_service() ){ return ; }

		$all_clients = $this->user_model->get_all_clients();
		
		if ( $all_clients == NULL )
		{
			// No clients in database
			log_message('debug','There are no clients in DB so far.');
			
			$this->load->view('service/header');
			$this->load->view('service/clients/no_clients');
			$this->load->view('service/footer');
			
			return ;
		}
		
		log_message('debug','Preparing clients table...');
		
		$this->table->set_heading( array('Meno', 'Priezvisko', 'Detail') );
		
		log_message('debug','Heading set, adding data...');
		
		foreach(  $all_clients as $client_row  )
		{
			
			log_message('debug','Adding client ' . $client_row->firstname . ' ' .  $client_row->lastname );
			
			$this->table->add_row( 
				array( 
					$client_row->firstname, 
					$client_row->lastname, 
					anchor(
						'service/clients_handler/client_detail/' . $client_row->id_user,
						'DETAIL',
						'title="detail"'
						) 
					) 
				);
		}

		log_message('debug','Table generation...');

		$clients_table = $this->table->generate();
		
		$data['clients_table'] = $clients_table;
		
		log_message('debug','Rendering...');
		
		$this->load->view('service/header');
		$this->load->view('service/clients/all_clients_index', $data);
		$this->load->view('service/footer')	;	
		
		return ;
	}
	
	
	function client_detail( $id_client )
	{
		// check if user logged
		if ( ! $this->authentify_service() ){ return ; }
		
		
		$client = $this->user_model->get_user_by_id( $id_client );
		
		if ( $client == NULL ) 
		{
			$data['id_client'] = $id_client;
			
			$this->load->view('service/header');
			$this->load->view('service/clients/no_such_client', $data);
			$this->load->view('service/footer')	;	
		
			return ;
		}
		
		// TODO: now we should confirm that user is really a client not admin or service
		
		// prepare table for client account data info
		$this->table->set_heading( 
			array('Priezvisko', 'Meno', 'Tel. číslo', 'Email', 'Pohlavie', 'Posledné prihlásenie') 
			);
		
		$this->table->add_row( 
				array( 
					$client['lastname'],
					$client['firstname'],
					$client['phone'],
					$client['email'],
					($client['gender'] == 0 ? 'Muž' : 'Žena'),
					$client['last_login'] 
					) 
				);
		
		$data['client_account_data_table'] = $this->table->generate();
		
		$this->table->clear();
		
		// get data from database for wallet info
		$data['client_actual_credit'] = $this->wallet_model->get_actual_credit_by_user_id( $id_client );
		
		$id_wallet = $this->wallet_model->get_wallet_id_by_user_id( $id_client );
		
		$data['id_wallet'] = $id_wallet; // appears as hidden input field in a form
		
		$id_wallet = $this->wallet_model->get_wallet_id_by_user_id( $id_client );
		
		/*************************** purchases ***********************************************************/
		$data['date_today']       = date("Y-m-d H:i:s", time());
		$ss_actual_service_user   = $this->session->userdata('actual_user');
		$data_actual_service_user = $this->user_model->get_user_by_id( $ss_actual_service_user['id_user'] );
		$data['receiver']    = $data_actual_service_user['nick'];
		
		$purchases = $this->purchase_model->get_purchases_and_user_data_by_wallet_id( $id_wallet );
		if ( $purchases != NULL)
		{
			// set flag to TRUE
			$data['show_purchases_flag'] = TRUE;
			
			$this->table->set_heading
				( 
				array('ID', 'Dátum dobitia', 'Hodnota', 'Prijal/a')
				);
				
			foreach(  $purchases as $single_purchase    )
			{
				$this->table->add_row( 
					array( 
						$single_purchase->id_purchase,
						$single_purchase->date,
						$single_purchase->value,
						$single_purchase->nick
					) 
				);
			};
			
			//generate table
			$data['purchases_table'] = $this->table->generate();
			// tidy up
			$this->table->clear();
		}else
		{
			// set flag to FALSE
			$data['show_purchases_flag'] = FALSE;
		}	
		
		/*************************** payments ***********************************************************/
		
		// get data from database for payment info
		//$data['client_payments'] = $this->payment_model->get_payments_by_wallet_id( $id_wallet );
		$payments = $this->payment_model->get_payments_by_wallet_id( $id_wallet );
		
		if ( $payments != NULL)
		{
			
			log_message('debug',' Select nonempty' );
			
			// set flag to TRUE
			$data['show_payments_flag'] = TRUE;
			
			// create convenient table
			$this->table->set_heading
				( 
				array('ID', 'Dátum platby', 'Typ platby', 'Vstup platný od', 'Vstup platný do')
				);
			
			foreach(  $payments as $single_payment    )
			{
				//log_message('debug','Adding '. $single_payment['id_payment'] . $single_payment['date'] . $single_payment['name']);
				$this->table->add_row( 
					array( 
						$single_payment->id_payment,
						$single_payment->date,
						$single_payment->name,
						$single_payment->datetime_from,
						$single_payment->datetime_to
					) 
				);
			};
			
			//generate table
			$data['payments_table'] = $this->table->generate();
			// tidy up
			$this->table->clear();
			
		}else
		{
			// set flag to FALSE
			$data['show_payments_flag'] = FALSE;
		}	

		log_message('debug','Rendering view for client...' . $client['lastname'] );
		
		$this->load->view('service/header');
		$this->load->view('service/clients/client_detail', $data);
		$this->load->view('client/purchase/purchases_view', $data);
		$this->load->view('client/payment/payments_view', $data);
		$this->load->view('service/footer');	
		
		return ;
	}
	
	public function create_purchase(){
		
		// check if user logged
		if ( ! $this->authentify_service() ){ return ; }		
		
		// setting validation for purchase value
		$this->form_validation->set_rules('value', 'Value', 'trim|required|numeric');
		
		// validate
		$validation_result = $this->form_validation->run();
		
		if ( $validation_result == FALSE )
		{
			$data['err_msg'] = 'Validácia nebola úspešná! Pravdepodobne nebolo korektne zadané číslo!';
			
			$this->load->view('service/header');
			$this->load->view('service/clients/create_purchase_fail');
			$this->load->view('service/footer');
			
			return ;
		}
		
		$purchase_value = $this->input->post('value');
		
		if (  $purchase_value != 0 )
		{
			log_message('debug', 'Purchase value parsed from form is ' . $purchase_value . '.');			
		}
		
		// everything OK
		$ss_actual_service_user = $this->session->userdata('actual_user');
		
		$this->purchase_model->create_purchase
			(
				$this->input->post('purchase_date'),
				$purchase_value,
				$ss_actual_service_user['id_user'],
				$this->input->post('id_wallet')
			);
				
		$this->wallet_model->add_credit_to_wallet
			(
				$this->input->post('id_wallet'),
				$purchase_value
			);
		
		$this->load->view('service/header');
		$this->load->view('service/clients/create_purchase_succ');
		$this->load->view('service/footer');
	}

}

/* End of file wallet.php */
/* Location: ./application/controllers/client/wallet.php */

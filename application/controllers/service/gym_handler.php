<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gym_handler extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('gym_model');
		//$this->load->model('wallet_model');
		//$this->load->model('payment_model');
		//$this->load->model('purchase_model');
		
		$this->load->helper(array('form','html'));
		
		//$this->load->library('table');
		$this->load->library('form_validation');
	}

	function index()
	{
		// check if user logged
		if ( ! $this->authentify_service() ){ return ; }

		$gym_data = $this->gym_model->get_gym_by_id();
		
		if ( $gym_data == NULL )
		{
			// No data for gym in database
			log_message('debug','There are no gym data in DB so far.');
			
			$this->load->view('service/header');
			$this->load->view('service/gym/no_gym_data');
			$this->load->view('service/footer');
			
			return ;
		}
		
		$data['name'] 			= $gym_data->name;
		$data['description'] 	= $gym_data->description;
		$data['phone_number'] 	= $gym_data->phone_number;
		$data['address'] 		= $gym_data->address;
		$data['price_list'] 	= $gym_data->price_list;
		
		log_message('debug','Rendering...');
		
		$this->load->view('service/header');
		$this->load->view('service/gym/gym_index', $data);
		$this->load->view('service/footer')	;	
		
		return ;
	}
	
	
	function change_data()
	{
		// check if user logged
		if ( ! $this->authentify_service() ){ return ; }
		
		$actual_gym_data = $this->gym_model->get_gym_by_id();
		
		$update_data = array();
		
		if( $this->input->post('name') != $actual_gym_data->name )
		{
			$update_data['name'] = $this->input->post('name') ;
		}
		
		if( $this->input->post('description') != $actual_gym_data->description )
		{
			$update_data['description'] = $this->input->post('description') ;
		}
		
		if( $this->input->post('phone') != $actual_gym_data->phone_number )
		{
			$update_data['phone_number'] = $this->input->post('phone')  ;
		}
		
		if( $this->input->post('address') != $actual_gym_data->address )
		{
			$update_data['address'] = $this->input->post('address') ;
		}
		
		if( $this->input->post('price_list') != $actual_gym_data->price_list )
		{
			$update_data['price_list'] = $this->input->post('price_list') ;
		}
		
		if ( empty($update_data) )
		{
			$this->load->view('service/header');
			$this->load->view('service/gym/no_changes_required');
			$this->load->view('service/footer')	;
			
			return ;
		}
		
		$this->gym_model->update_gym_by_id_and_data( 1 , $update_data );
		
		$this->load->view('service/header');
		$this->load->view('service/gym/change_succ');
		$this->load->view('service/footer');
	}

}

/* End of file wallet.php */
/* Location: ./application/controllers/client/wallet.php */

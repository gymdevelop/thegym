<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('user_model');
		$this->load->model('wallet_model');
		$this->load->model('payment_model');
		$this->load->model('purchase_model');
		
		$this->load->helper(array('form','html'));
		
		$this->load->library('table');
		$this->load->library('form_validation');
	}

	function index()
	{
		// check if user logged
		if ( ! $this->authentify_any() ){ return ; }
		
		// retrieve purchase data for this user
		$session_data = $this->session->userdata('actual_user');

		$actual_user_wallet_id = $this->wallet_model->get_wallet_id_by_user_id( $session_data['id_user'] );
		
		$atual_user_payments = $this->payment_model->get_payments_by_wallet_id( $actual_user_wallet_id );
		
		if ( $atual_user_payments != NULL )
		{
			$this->table->set_heading
				( 
				array('ID', 'Dátum platby', 'Typ platby', 'Vstup platný od', 'Vstup platný do')
				);
				
			foreach(  $atual_user_payments as $single_payment    )
			{
				$this->table->add_row( 
					array( 
						$single_payment->id_payment,
						$single_payment->date,
						$single_payment->name,
						$single_payment->datetime_from,
						$single_payment->datetime_to
					) 
				);
			};
			
			//generate table
			$data['payments_table'] = $this->table->generate();
			
			// tidy up
			$this->table->clear();
			
			// set flag to TRUE, show purchases
			$data['show_payments_flag'] = TRUE;
		}else
		{
			// set flag to FALSE, NO purchases
			$data['show_payments_flag'] = FALSE;
		}
		
		// render
		$this->load->view('client/header', $data);
		$this->load->view('client/payment/payments_view', $data);
		$this->load->view('client/footer', $data);
	}
	
}

/* End of file purchase.php */
/* Location: ./application/controllers/client/purchase.php */	

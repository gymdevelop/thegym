<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wallet extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('wallet_model');
	}

	function index()
	{
		// check if user logged
		if ( ! $this->authentify_any() ){ return ; }
		
		$session_data = $this->session->userdata('actual_user');

		$data['actual_credit'] = $this->wallet_model->get_actual_credit_by_user_id( $session_data['id_user'] );
		
		$this->load->view('client/header', $data);
		$this->load->view('client/wallet/wallet', $data);
		$this->load->view('client/footer', $data);
	}
}

/* End of file wallet.php */
/* Location: ./application/controllers/client/wallet.php */
